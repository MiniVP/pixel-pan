#ifndef DHT_PIN
#define DHT_PIN 10
#endif
#ifndef UV_PIN
#define UV_PIN A1
#endif
#ifndef UV_EN_PIN
#define UV_EN_PIN 10
#endif
#ifndef REF_PIN
#define REF_PIN A0
#endif

#include "Pan.h"
#include "DHT.h"
#include "BMP085.h"
#include <Wire.h>
#include <Tsl2561.h>
#include <Tsl2561Util.h>

class PixelPan: public Pan {
  public:
    PixelPan(): Pan(), Tsl(Wire) {
    }
    void begin() {
      Pan::begin();
      dht.setup(DHT_PIN);
      pinMode(UV_EN_PIN, OUTPUT);
      bmp.init();
      while (!Tsl.begin());
      Tsl.on();
      uint8_t id;
      Tsl.id(id);
      tslCS = Tsl2561::packageCS(id);
    }

    String buildOutput() {
      // Prepare the ML8511
      digitalWrite(UV_EN_PIN, HIGH);
      delay(50);

      output = "";
      #if USE_RTC == 1
        // ASIA=YYYY-MM-DD; US=MM/DD/YYYY; WORLD=DD-MM-YYYY
        output.concat(F("[TIME]"));
        output.concat(rtc.formatDate(RTCC_DATE_ASIA));
        output.concat('T');
        output.concat(rtc.formatTime());
      #endif

      // Temperature from the DHT, in °C
      float dhtTemp = dht.getTemperature();
      // Relative humidity from the DHT in %
      float humidity = dht.getHumidity();
      // TODO: This reads the sensor twice due to a protected method; maybe update the DHT lib?

      // The status only gets updated after reading the sensor
      if (!dht.getStatus()) {
        output.concat(F("[TEMP]"));
        output.concat(dhtTemp);
        output.concat(F("[HUMIDITY]"));
        output.concat(humidity);
      }

      // UV data from the ML8511, in mW.cm^-2
      output.concat(F("[UV]"));
      output.concat(mapfloat(3.3 / averageAnalogRead(REF_PIN) * averageAnalogRead(UV_PIN), 0.976, 2.8, 0.0, 15.0));
      digitalWrite(UV_EN_PIN, LOW);

      // Temperature from the BMP180 in °C
      output.concat(F("[PRESSURE_TEMP]"));
      output.concat(bmp.bmp085GetTemperature(bmp.bmp085ReadUT()));
      // Pressure from the BMP180 in Pa
      output.concat(F("[PRESSURE]"));
      output.concat(bmp.bmp085GetPressure(bmp.bmp085ReadUP()));

      // Illuminance from the TSL2561 in millilux (mlx)
      uint16_t scaledFull = 0xffff, scaledIr = 0xffff;
      uint32_t full = 0xffffffff, ir = 0xffffffff, milliLux = 0xffffffff;
      bool gain = false;
      Tsl2561::exposure_t exposure = Tsl2561::EXP_OFF;
      if (
          // Read luminosity and adjust gain as needed
          Tsl2561Util::autoGain(Tsl, gain, exposure, scaledFull, scaledIr)
          // Normalize luminosity to look like we are using 402ms exposure and 16X gain, fail if saturated
          && Tsl2561Util::normalizedLuminosity(gain, exposure, full = scaledFull, ir = scaledIr)
          // Convert raw data to millilux
          && Tsl2561Util::milliLux(full, ir, milliLux, tslCS, 5)
      ) {
        output.concat(F("[LIGHT]"));
        output.concat(milliLux);
      }
      // Even if everything fails, include the raw CH0 and CH1 data for debugging
      output.concat(F("[FULL]"));
      output.concat(full);
      output.concat(F("[IR]"));
      output.concat(ir);

      return output;
    }

  protected:
    DHT dht;
    BMP085 bmp;
    Tsl2561 Tsl;

    // Whether or not this TSL2561 has the CS packaging.
    // This changes coefficients when computing lux data.
    // Obtained from the chip's ID on startup.
    bool tslCS;

    // Average of multiple readings on an analog pin to try to enhance precision
    int averageAnalogRead(int pinToRead)
    {
      byte numberOfReadings = 8;
      unsigned int runningValue = 0;

      for(int x = 0 ; x < numberOfReadings ; x++)
        runningValue += analogRead(pinToRead);
      runningValue /= numberOfReadings;
      return runningValue;
    }

    // The Arduino Map function but for floats
    // From: http://forum.arduino.cc/index.php?topic=3922.0
    float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
    {
      return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
};

PixelPan pixelPan;

void setup() {
  pixelPan.begin();
}

void loop() {
  pixelPan.loop();
}
