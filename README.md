![MiniVP Pixel-Pan](img/banner.png)

Also see the [global Pan family setup instructions](lib/Pan/README.md) for more information on how to use this station.

## Components

* Arduino (tested with Uno R3 and Pro Mini)
* DHT-11, DHT-21 (AM2301) or DHT-22 (AM2302, AM2320, AM2321, RHT03) temperature and humidity sensor
* TSL2561 luminosity sensor
* ML8511 ultraviolet sensor
* BMP180 pressure sensor
* Optional: PCF8563 real-time clock
* Optional: SD or microSD card reader
* Optional: LED

## Build options

Build flags are available in `platformio.ini` to toggle optional features and set some settings on the station. The flags specific to this model of Pan weather station are listed below along with the pinout.

To learn more about common configuration flags in the Pan weather station family, refer to the [common documentation](../Pan/README.md#build-flags).

## Pinout

Arduino Uno R3 | Configuration flag | Components
-------------- | ------------------ | ----------
3.3V           | —                  | DHT pin 1 (left), ML8511 3V3, BMP180 VIN, TSL2561 VCC
GND            | —                  | DHT11/22 pin 4 (right) or AM2320 pin 3 (middle right), ML8511 GND, BMP180 GND, TSL2561 GND, LED GND (optional)
SDA            | —                  | BMP180 SDA, TSL2561 SDA, with pull-up resistors
SCL            | —                  | BMP180 SCL, TSL2561 SCL, with pull-up resistors
A0             | `analog_ref_pin`   | Arduino 3V3
A1             | `uv_out_pin`       | ML8511 OUT
D5~            | `uv_en_pin`        | ML8511 EN
D2             | `dht_pin`          | DHT pin 2 (middle left)
D13~           | `led_pin`          | LED (optional)

If the `sd` configuration flag is enabled, the following additional pins are required:

Arduino UNO R3 | Configuration flag | Components
-------------- | ------------------ | ----------
3.3V           | —                  | SD 3V3
GND            | —                  | SD GND
D4~            | `sd_cs_pin`        | SD CS
D11~           | —                  | SD MOSI
D12~           | —                  | SD MISO
D13~           | —                  | SD CLK

If the `rtc` configuration flag is enabled, the following additional pins are required:

Arduino UNO R3 | Configuration flag | Components
-------------- | ------------------ | ----------
3.3V           | —                  | RTC 3V3
GND            | —                  | RTC GND
SDA            | —                  | RTC SDA
SCL            | —                  | RTC SCL

## Output format

The Arduino outputs a single CRLF-terminated line for each measurement, a series of key-value pairs as `[KEY]VALUE`:

Key             | Type   | Unit   | Description
--------------- | ------ | ------ | ----------------------------------------------------------------
`TEMP`          | float  | °C     | Temperature from the DHT sensor.
`HUMIDITY`      | float  | %RH    | Relative humidity from the DHT sensor.
`UV`            | float  | mW/cm² | Irradiance at 365nm from the ML8511 sensor.
`PRESSURE_TEMP` | float  | °C     | Temperature from the BMP sensor.
`PRESSURE`      | uint32 | Pa     | Pressure from the BMP sensor.
`LIGHT`         | uint32 | mlx    | Illuminance from the TSL2561.
`FULL`          | uint32 | —      | Raw data from the TSL2561 Channel 0 sensor for testing purposes.
`IR`            | uint32 | —      | Raw data from the TSL2561 Channel 1 sensor for testing purposes.

Example: `[TEMP]19.60[HUMIDITY]35.40[UV]0.30[PRESSURE_TEMP]19.20[PRESSURE]98803[LIGHT]65406[FULL]2402[IR]394`

## Failure modes

* If a checksum error occurs when retrieving data from the DHT, or the retrieval times out, both `TEMP` and `HUMIDITY` will not be included in the output.
* If an error such as an I2C error occurs when retrieving data from the TSL2561, `UV` will not be included in the output.
* Pixel-Pan is unable to detect ML8511 measurement errors and may output incorrect data in those cases, although it will still be a valid float.
* Pixel-Pan will be stuck waiting if the BMP sensor gets disconnected and cannot recover automatically.
